#ifndef __CAN_OPEN_H
#define __CAN_OPEN_H

#include "stm32f0xx_hal.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern CAN_HandleTypeDef hcan;
extern CAN_TxHeaderTypeDef pHeader;
extern CAN_RxHeaderTypeDef pRxHeader;
extern uint32_t TxMailbox;
extern uint32_t a, r;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern void can_hardware_init(void);
CAN_HandleTypeDef *can_get_handle_ptr(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __CAN_OPEN_H */
