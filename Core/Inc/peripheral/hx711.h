#ifndef __HX711_H
#define __HX711_H

#include "stm32f0xx_hal.h"
#include <stdbool.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MAX_AVARAGE_BUFF           10

#define GAIN                       128      // gain factor 128 , 32 , 64
#define UNIT                       -1070     // 200 - g | 2000 -dg | 200000 -kg -949
#define OFFSET                     8392500

#define SENS_A_GPIO_CLK            GPIOF
#define SENS_A_PIN_CLK             GPIO_PIN_1

#define SENS_A_GPIO_DATA           GPIOF
#define SENS_A_PIN_DATA            GPIO_PIN_0

// sensor B
#define SENS_B_GPIO_CLK            GPIOB
#define SENS_B_PIN_CLK             GPIO_PIN_0

#define SENS_B_GPIO_DATA           GPIOB
#define SENS_B_PIN_DATA            GPIO_PIN_1

typedef struct _hx711_s
{	
	GPIO_TypeDef* gpio_clk;
	GPIO_TypeDef* gpio_data;
	uint16_t pin_clk;
	uint16_t pin_data;
	
	int32_t offset;
	
	int16_t unit;				// 200 - g | 2000 -dg | 200000 -kg
	
	uint16_t gain;				// gain factor 128 , 32 , 64

	uint8_t read_times;

	int32_t avarage_buff[MAX_AVARAGE_BUFF];

	int32_t avarage_weight;

	int32_t weight;

	bool err_conn;

}hx711_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

hx711_t *hx711_crreate(void);
int32_t hx711_get_weight(hx711_t *data);
int32_t hx711_get_average_weight(hx711_t *data, uint8_t times);
bool hx711_tare(hx711_t *data, uint8_t times);
void hx711_sleep_mode(hx711_t *data);
void hx711_wake_up(hx711_t *data);




#endif /* __HX711_H */