#ifndef __LOG_MODULES_H
#define __LOG_MODULES_H

#include "settings_log.h"
#include "log.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern log_module_t *log_can_msg;
extern log_module_t *log_can_msg_ctrl;

extern log_module_t *log_controller;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void init_log_modules();


#endif /* __LOG_MODULES_H */
