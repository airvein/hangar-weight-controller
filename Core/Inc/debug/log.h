#ifndef __LOG_H
#define __LOG_H

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MODULE_NAME_LEN     50

#define ASSERT_LOG_ERROR_G(condition, msg, ...) \
        do { if(condition) log_error_g(msg, ##__VA_ARGS__); }while(false)

#define ASSERT_LOG_WARNING_G(condition, msg, ...) \
        do { if(condition) log_warning_g(msg, ##__VA_ARGS__); }while(false)

#define ASSERT_LOG_INFO_G(condition, msg, ...) \
        do { if(condition) log_info_g(msg, ##__VA_ARGS__); }while(false)

#define ASSERT_LOG_DEBUG_G(condition, msg, ...) \
        do { if(condition) log_debug_g(msg, ##__VA_ARGS__); }while(false)

#define ASSERT_LOG_ERROR_M(condition, module, msg, ...) \
    do { if(condition) log_error_m(module, msg, ##__VA_ARGS__); }while(false)

#define ASSERT_LOG_WARNING_M(condition, module, msg, ...) \
    do { if(condition) log_warning_m(module, msg, ##__VA_ARGS__); }while(false)

#define ASSERT_LOG_INFO_M(condition, module, msg, ...) \
    do { if(condition) log_info_m(module, msg, ##__VA_ARGS__); }while(false)

#define ASSERT_LOG_DEBUG_M(condition, module, msg, ...) \
    do { if(condition) log_debug_m(module, msg, ##__VA_ARGS__); }while(false)   

#define LOG_ENA_ERR(enable)     (enable == 1 ? LOG_LVL_ERROR : 0)
#define LOG_ENA_WARN(enable)    (enable == 1 ? LOG_LVL_WARNING : 0)
#define LOG_ENA_INFO(enable)    (enable == 1 ? LOG_LVL_INFO : 0)
#define LOG_ENA_DBG(enable)     (enable == 1 ? LOG_LVL_DEBUG : 0)   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _log_lvl_e
{
    LOG_LVL_DISABLE = 0,
    LOG_LVL_ERROR = 1,
    LOG_LVL_WARNING = 2,
    LOG_LVL_INFO = 4,
    LOG_LVL_DEBUG = 8,
    LOG_LVL_ALL = 
        LOG_LVL_ERROR | LOG_LVL_WARNING | LOG_LVL_INFO | LOG_LVL_DEBUG

} log_lvl_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _log_module_s
{
    char name[MODULE_NAME_LEN];
    uint8_t level;

} log_module_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool log_register_printf_clbk(int(*printf_clbk)(const char *fmt,...));

//enable / disable timestamp in log msg

bool log_set_timestamp_var_ptr(uint32_t *timestamp_ptr);

void log_clear_timestamp_var_ptr(void);

//global_log functions

bool log_set_global_log_level(log_lvl_t log_lvl);

uint8_t log_get_global_log_level(void);

bool log_debug_g(const char *log_msg, ...);

bool log_info_g(const char *log_msg, ...);

bool log_warning_g(const char *log_msg, ...);

bool log_error_g(const char *log_msg, ...);

//module_log functions

log_module_t *log_module_create();

bool log_module_init(log_module_t *log_module, char *module_name, 
                     log_lvl_t log_lvl);

bool log_debug_m(log_module_t *log_module, const char *log_msg, ...);

bool log_info_m(log_module_t *log_module, const char *log_msg, ...);

bool log_warning_m(log_module_t *log_module, const char *log_msg, ...);

int log_error_m(log_module_t *log_module, const char *log_msg, ...);


#endif /* __LOG_H */
