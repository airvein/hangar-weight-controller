#ifndef __UART_OPEN_H
#define __UART_OPEN_H

#include <stm32f0xx_hal.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern USART_HandleTypeDef huart2;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern void uart_hardware_init(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __UART_OPEN_H */
