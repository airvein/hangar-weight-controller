#ifndef __CARGO_CANBUS_MESSAGE_H
#define __CARGO_CANBUS_MESSAGE_H

#include "can/can_msg.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum 
{
    CARGO_MSG_MIN,

    CARGO_MSG_TARE = CARGO_MSG_MIN,
    CARGO_MSG_WEIGHT,
    CARGO_MSG_CTRL_PRESENCE,

    CARGO_MSG_MAX = CARGO_MSG_CTRL_PRESENCE,
    CARGO_MSG_CNT

} cargo_msg_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    CARGO_REQ_TARE = CARGO_MSG_TARE,
    CARGO_REQ_WEIGHT = CARGO_MSG_WEIGHT
} cargo_req_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    CARGO_TARE_ERR,
    CARGO_TARE_OK

} cargo_tare_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cargo_canbus_messages_init(void);
can_msg_t *cargo_canbus_messages_get_array(void);

bool cargo_canbus_send_weight(int32_t payload);

bool cargo_canbus_tare_response(cargo_tare_t res);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CARGO_CANBUS_MESSAGE_H */