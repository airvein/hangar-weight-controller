#ifndef __APP_PERIPHERAL_INIT_H
#define __APP_PERIPHERAL_INIT_H

#include <stdint.h>
#include <string.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


void app_peripheral_init(void);

int32_t app_peripheral_get_cargo_weight(uint8_t read_times);
void    app_peripheral_tare_cargo_weight(uint8_t read_times);
void    app_peripheral_weight_sleep_modules(void);
void    app_peripheral_weight_wake_up_modules(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_PERIPHERAL_INIT_H */