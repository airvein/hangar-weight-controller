#include <stdint.h>
#include "communication_interface\cargo_canbus_message.h"
#include "debug/log_modules.h"
#include "can/can_open.h"
#include "app_peripheral/app_peripheral_init.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CH_CAN_MSG_ID_TARE              0x000001
#define CH_CAN_MSG_ID_WEIGHT            0x000002
#define CH_CAN_MSG_ID_CTRL_PRESENCE     0x000011

#define HiByteU16(bytes) (bytes >> 8)
#define LoByteU16(bytes) (bytes & 0x00FF)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct msg_init_param
{
    uint32_t id;

    can_msg_handler_cb handler_cb;
    can_msg_remote_resp_cb response_cb;
}; 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_t message_array[CARGO_MSG_CNT] = { 0 };


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t msg_tare_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t msg_tare_response_cb(void);

static can_msg_handle_t msg_weight_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t msg_weight_response_cb(void);

static can_msg_handle_t msg_ctrl_presence_handler_cb(can_msg_t *msg);
static can_msg_remote_resp_data_t msg_ctrl_presence_response_cb(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct msg_init_param msg_init_array[CARGO_MSG_CNT] = 
    {
        [CARGO_MSG_TARE] = 
            {   .id = CH_CAN_MSG_ID_TARE,
                .handler_cb = msg_tare_handler_cb,
                .response_cb = msg_tare_response_cb },
        [CARGO_MSG_WEIGHT] = 
            {   .id = CH_CAN_MSG_ID_WEIGHT,
                .handler_cb = msg_weight_handler_cb,
                .response_cb = msg_weight_response_cb },
        [CARGO_MSG_CTRL_PRESENCE] = 
            {   .id = CH_CAN_MSG_ID_CTRL_PRESENCE,
                .handler_cb = msg_ctrl_presence_handler_cb,
                .response_cb = msg_ctrl_presence_response_cb }
    } ;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cargo_canbus_messages_init(void)
{
    CAN_HandleTypeDef *hcan = can_get_handle_ptr();
    for(int msg_id = 0; msg_id < CARGO_MSG_CNT; msg_id++)
    {
        can_msg_init(&message_array[msg_id], hcan, 
                msg_init_array[msg_id].id,
                msg_init_array[msg_id].handler_cb, 
                msg_init_array[msg_id].response_cb);
    }
    __HAL_RCC_GPIOB_CLK_ENABLE();
    GPIO_InitTypeDef gpio;
    gpio.Pin = GPIO_PIN_3;
    gpio.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOB, &gpio);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

can_msg_t *cargo_canbus_messages_get_array(void)
{
    return message_array;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cargo_canbus_tare_response(cargo_tare_t res)
{
    uint32_t msg_id = CARGO_MSG_TARE;
    uint8_t data[1] = {res};

    return can_msg_send_data(&message_array[msg_id], data, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool cargo_canbus_send_weight(int32_t payload)
{
    union weight2canbusdata
    {
        int32_t weight;
        uint8_t canbus_data[sizeof(int32_t)];
    };

    uint32_t msg_id = CARGO_MSG_WEIGHT;
    union weight2canbusdata data = 
    {
        .weight = payload
    };

    return can_msg_send_data(&message_array[msg_id], data.canbus_data, 4);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t msg_tare_handler_cb(can_msg_t *msg)
{
    app_peripheral_tare_cargo_weight(10);

    cargo_tare_t res = CARGO_TARE_ERR;
    int32_t weight = app_peripheral_get_cargo_weight(1);
    
    if( weight == 0)
    {
        res = CARGO_TARE_OK;
    }
    cargo_canbus_tare_response(res);
    cargo_canbus_send_weight(weight);
    
    log_info_m(log_controller,"Tare process done: [%s] / weight = %d",
                               res == CARGO_TARE_OK ? "OK" : "ERR", weight);
    
    return CAN_MSG_HANDLE_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t msg_tare_response_cb(void)
{   
    can_msg_remote_resp_data_t resp_data =
    {
        .data = NULL,
        .data_len = 0
    };

    return resp_data;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t msg_weight_handler_cb(can_msg_t *msg)
{
    int32_t weight = app_peripheral_get_cargo_weight(1)/2;
    bool res = cargo_canbus_send_weight(weight);
    log_info_m(log_controller,"Weight process done => weight = %d / [ %s ]", 
                weight, res ? "SEND" : "SEND_ERR");
    
    return CAN_MSG_HANDLE_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t msg_weight_response_cb(void)
{   
    can_msg_remote_resp_data_t resp_data =
    {
        .data = NULL,
        .data_len = 0
    };

    return resp_data;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_handle_t msg_ctrl_presence_handler_cb(can_msg_t *msg)
{
    log_error_g("msg_ctrl_presence_handler_cb:%d", msg->id);
    return CAN_MSG_HANDLE_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_remote_resp_data_t msg_ctrl_presence_response_cb(void)
{   
    log_info_m(log_controller, "Presence request response!");
    static uint8_t presence_data[1] = { 255 };
    can_msg_remote_resp_data_t resp_data =
    {
        .data = presence_data,
        .data_len = 1
    };

    return resp_data;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|