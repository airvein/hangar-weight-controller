#include "communication_interface/cargo_canbus.h"
#include "communication_interface/cargo_canbus_message.h"
#include "debug/log_modules.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static can_msg_ctrl_t cargo_canbus = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint16_t msg_added_num = 0;

static can_msg_t *cargo_messages[CARGO_MSG_CNT] = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void add_msg_ptr_to_messages_array(can_msg_t *msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cargo_canbus_init(void)
{
    cargo_canbus_messages_init();
    can_msg_t *cargo_messages_ptr = cargo_canbus_messages_get_array();

    for(int msg_id = 0; msg_id < CARGO_MSG_CNT; msg_id++)
    {
        add_msg_ptr_to_messages_array(&cargo_messages_ptr[msg_id]);
    }

    if(CARGO_MSG_CNT != msg_added_num)
    {
        log_error_m(log_controller, 
                    "Not equal amount of cargo can_msg initalization "
                    "/ timeout array size! [ %s : %s ]",
                    __FILE__, __func__);
        while(1); //err infinite loop
    }
    else
    {
        can_msg_ctrl_init(&cargo_canbus, cargo_messages,
                          msg_added_num);
        log_info_m(log_controller, 
                    "Application cargo canbus initialized succesfully!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cargo_canbus_msg_execute_handler(void)
{
    can_msg_ctrl_execute_handler(&cargo_canbus);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cargo_canbus_msg_order_handler(can_msg_recv_t recv_msg)
{
    can_msg_ctrl_order_handler(&cargo_canbus, recv_msg);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void add_msg_ptr_to_messages_array(can_msg_t *msg)
{
    if(msg == NULL)
    {
        log_error_m(log_controller, 
            "Try add NULL ptr can_msg to cargo canbus messages "
            "array with [ %d ] id !", msg_added_num);
        return ;
    }

    cargo_messages[msg_added_num++] = msg;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|