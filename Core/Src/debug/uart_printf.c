/**
  ******************************************************************************
  * @file           : uart_printf.c
  * @brief          : Printf functions via serial (USART)
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#include "debug/uart_printf.h"
#include "string.h"



#define SERVICE_DEBUG

extern USART_HandleTypeDef huart2;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void vprint(const char *fmt, va_list argp)
{
    char string[255];

    if(0 < vsprintf(string,fmt,argp))
    {
        HAL_USART_Transmit(&huart2, (uint8_t*)string, strlen(string), 100);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int uart_printf(const char *fmt, ...)
{
	#ifdef SERVICE_DEBUG
		va_list argp;
		va_start(argp, fmt);
		vprint(fmt, argp);
		va_end(argp);
	#endif
	return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
