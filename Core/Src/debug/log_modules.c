#include "debug/log_modules.h"

#include <stdio.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_MODULE_INIT(log_module, name, settings)	\
	{ \
		log_module_init(log_module, \
						name, settings);  \
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static log_module_t sv_log_can_msg = { 0 };
static log_module_t sv_log_can_msg_ctrl = { 0 };

static log_module_t sv_log_controller = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

log_module_t *log_can_msg = &sv_log_can_msg;
log_module_t *log_can_msg_ctrl = &sv_log_can_msg_ctrl;

log_module_t *log_controller = &sv_log_controller;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void init_log_modules()
{
	LOG_MODULE_INIT(log_controller, LOG_CTRL_NAME, LOG_CONTROLLER_SETTINGS);

	LOG_MODULE_INIT(log_can_msg, "can_msg", LOG_CAN_MSG_SETTINGS);
	LOG_MODULE_INIT(log_can_msg_ctrl, "can_msg_ctrl", LOG_CAN_MSG_SETTINGS);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
