#include "peripheral/hx711.h"

#include "debug/log_modules.h"

#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint32_t hx711_read_value(hx711_t *data);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

hx711_t *hx711_crreate()
{
    return (hx711_t*)malloc(sizeof(hx711_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int32_t hx711_get_weight(hx711_t *data)
{
    int32_t buff = hx711_read_value(data);

    buff = (buff - data->offset) / data->unit;
    
    data->weight = buff;

    return data->weight;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int32_t hx711_get_average_weight(hx711_t *data, uint8_t times)
{
    if(data->read_times >= times)
    {
        data->read_times = 0;
    }

    data->read_times++;
    data->avarage_buff[data->read_times - 1] = hx711_get_weight(data);
    
    int32_t buff = 0;

    for(uint8_t i = 0; i < times ; i++)
    {
        buff += data->avarage_buff[i];
    }

    data->avarage_weight = buff / times;
    return data->avarage_weight;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hx711_tare(hx711_t *data, uint8_t times)
{
    int32_t sum = 0;

    for(uint8_t i = 0 ; i < times ; i++)
    {
        sum += hx711_read_value(data);
    }

    sum = sum/times;

    data->offset = sum;

    return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hx711_sleep_mode(hx711_t *data)
{
    HAL_GPIO_WritePin(data->gpio_clk, data->pin_clk, GPIO_PIN_SET);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hx711_wake_up(hx711_t *data)
{
    HAL_GPIO_WritePin(data->gpio_clk, data->pin_clk, GPIO_PIN_RESET);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint32_t hx711_read_value(hx711_t *data)
{
    __disable_irq();
    
    uint32_t comm_wait_time = 10000000;
    int32_t buffer;
    buffer = 0;
    data->err_conn = false;

    log_debug_m(log_controller, "Waiting for weight pin data...");
    while(HAL_GPIO_ReadPin(data->gpio_data, data->pin_data) == 1 && comm_wait_time > 0)
    {
        comm_wait_time--;
    }

    ASSERT_LOG_WARNING_M(comm_wait_time == 0, log_controller, 
                        "Weight sesnor communication timeout!");


    for (uint8_t i = 0; i < 24; i++)
    {
    	HAL_GPIO_WritePin(data->gpio_clk, data->pin_clk, GPIO_PIN_SET);

        if (HAL_GPIO_ReadPin(data->gpio_data, data->pin_data))
        {
            buffer |= (1 << (23 - i));
        }

        HAL_GPIO_WritePin(data->gpio_clk, data->pin_clk, GPIO_PIN_RESET);
    }
    
    for (int i = 0; i < data->gain; i++)
    {
    	HAL_GPIO_WritePin(data->gpio_clk, data->pin_clk, GPIO_PIN_SET);
    	HAL_GPIO_WritePin(data->gpio_clk, data->pin_clk, GPIO_PIN_RESET);
    }

    __enable_irq();

    return buffer;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    			E X A M P L E
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// int main(void)
// {
// hx711_t *weight1 = NULL;

// weight1 = hx711_crreate();

// weight1->gpio_clk = GPIO_CLK;
// weight1->gpio_data = GPIO_DATA;

// weight1->pin_clk = PIN_CLK;
// weight1->pin_data = PIN_DATA;

// weight1->gain = GAIN;
// weight1->unit = UNIT;
// weight1->offset = OFFSET;
// weight1->avarage_weight = 0;
// weight1->read_times = 0;
// memset(weight1->avarage_buff, 0, 100 * sizeof(int));

// GPIO_InitTypeDef GPIO_InitStruct;

// GPIO_InitStruct.Pin = PIN_CLK;
// GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
// GPIO_InitStruct.Pull = GPIO_NOPULL;
// GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
// HAL_GPIO_Init(GPIO_CLK, &GPIO_InitStruct);

// GPIO_InitStruct.Pin = PIN_DATA;
// GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
// GPIO_InitStruct.Pull = GPIO_PULLUP;
// GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
// HAL_GPIO_Init(GPIO_DATA, &GPIO_InitStruct);

// HAL_GPIO_WritePin(GPIO_DATA, PIN_DATA, GPIO_PIN_SET);
// HAL_Delay(50);
// HAL_GPIO_WritePin(GPIO_CLK, PIN_CLK, GPIO_PIN_RESET);

// hx711_tare(weight1, 10);

// 	while (1)
// 	{	
// 		log_debug_g("weight = [ %d ]", hx711_get_average_weight(weight1, 10));
// 	}
//}