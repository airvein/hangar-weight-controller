#include "app_peripheral/app_peripheral_init.h"
#include "peripheral/hx711.h"
#include "debug/log_modules.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define INIT_CARGO_WEIGHT_TARE_NUM  10

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

hx711_t sv_cargo_weight_sensor_a = { 0 };
hx711_t sv_cargo_weight_sensor_b = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void weight_sensors_init(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_peripheral_init(void)
{
   weight_sensors_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int32_t app_peripheral_get_cargo_weight(uint8_t read_times)
{
    int32_t result1 = 0;
    int32_t result2 = 0;

    // app_peripheral_weight_wake_up_modules();
    hx711_wake_up(&sv_cargo_weight_sensor_a);
    result1 = hx711_get_average_weight(&sv_cargo_weight_sensor_a, read_times);
    hx711_sleep_mode(&sv_cargo_weight_sensor_a);

    hx711_wake_up(&sv_cargo_weight_sensor_b);
    result2 = hx711_get_average_weight(&sv_cargo_weight_sensor_b, read_times);
    hx711_sleep_mode(&sv_cargo_weight_sensor_b);

    app_peripheral_weight_sleep_modules();
    log_warning_g("%d %d", result1, result2);

    return (result1 + result2);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_peripheral_tare_cargo_weight(uint8_t read_times)
{
    hx711_wake_up(&sv_cargo_weight_sensor_a);
    hx711_tare(&sv_cargo_weight_sensor_a, read_times);
    hx711_sleep_mode(&sv_cargo_weight_sensor_a);

    hx711_wake_up(&sv_cargo_weight_sensor_b);
    hx711_tare(&sv_cargo_weight_sensor_b, read_times);
    hx711_sleep_mode(&sv_cargo_weight_sensor_b);

    app_peripheral_weight_sleep_modules();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_peripheral_weight_sleep_modules(void)
{
    hx711_sleep_mode(&sv_cargo_weight_sensor_a);
    hx711_sleep_mode(&sv_cargo_weight_sensor_b);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_peripheral_weight_wake_up_modules(void)
{
    hx711_wake_up(&sv_cargo_weight_sensor_a);
    hx711_wake_up(&sv_cargo_weight_sensor_b);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void weight_sensors_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    sv_cargo_weight_sensor_a.gpio_clk = SENS_A_GPIO_CLK;
    sv_cargo_weight_sensor_a.gpio_data = SENS_A_GPIO_DATA;

    sv_cargo_weight_sensor_a.pin_clk = SENS_A_PIN_CLK;
    sv_cargo_weight_sensor_a.pin_data = SENS_A_PIN_DATA;

    sv_cargo_weight_sensor_a.gain = GAIN;
    sv_cargo_weight_sensor_a.unit = UNIT;
    sv_cargo_weight_sensor_a.offset = 0;
    sv_cargo_weight_sensor_a.avarage_weight = 0;
    sv_cargo_weight_sensor_a.read_times = 0;
    memset(sv_cargo_weight_sensor_a.avarage_buff,
                                        0, MAX_AVARAGE_BUFF * sizeof(int));

    GPIO_InitStruct.Pin = SENS_A_PIN_CLK;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SENS_A_GPIO_CLK, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = SENS_A_PIN_DATA;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SENS_A_GPIO_DATA, &GPIO_InitStruct);

    HAL_GPIO_WritePin(SENS_A_GPIO_DATA, SENS_A_PIN_DATA, GPIO_PIN_SET);
    HAL_Delay(50);
    HAL_GPIO_WritePin(SENS_A_GPIO_CLK, SENS_A_PIN_CLK, GPIO_PIN_RESET);

    log_info_m(log_controller, "Weight sensors tare1");
    hx711_tare(&sv_cargo_weight_sensor_a, INIT_CARGO_WEIGHT_TARE_NUM);
    hx711_get_average_weight(&sv_cargo_weight_sensor_a, 1);
    hx711_sleep_mode(&sv_cargo_weight_sensor_a);

    sv_cargo_weight_sensor_b.gpio_clk = SENS_B_GPIO_CLK;
    sv_cargo_weight_sensor_b.gpio_data = SENS_B_GPIO_DATA;

    sv_cargo_weight_sensor_b.pin_clk = SENS_B_PIN_CLK;
    sv_cargo_weight_sensor_b.pin_data = SENS_B_PIN_DATA;

    sv_cargo_weight_sensor_b.gain = GAIN;
    sv_cargo_weight_sensor_b.unit = UNIT;
    sv_cargo_weight_sensor_b.offset = 0;
    sv_cargo_weight_sensor_b.avarage_weight = 0;
    sv_cargo_weight_sensor_b.read_times = 0;
    memset(sv_cargo_weight_sensor_b.avarage_buff,
                                        0, MAX_AVARAGE_BUFF * sizeof(int));


    GPIO_InitStruct.Pin = SENS_B_PIN_CLK;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SENS_B_GPIO_CLK, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = SENS_B_PIN_DATA;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SENS_B_GPIO_DATA, &GPIO_InitStruct);

    HAL_GPIO_WritePin(SENS_B_GPIO_DATA, SENS_B_PIN_DATA, GPIO_PIN_SET);
    HAL_Delay(50);
    HAL_GPIO_WritePin(SENS_B_GPIO_CLK, SENS_B_PIN_CLK, GPIO_PIN_RESET);

    log_info_m(log_controller, "Weight sensors tare2");
    hx711_tare(&sv_cargo_weight_sensor_b, INIT_CARGO_WEIGHT_TARE_NUM);
    hx711_get_average_weight(&sv_cargo_weight_sensor_b, 1);    
    hx711_sleep_mode(&sv_cargo_weight_sensor_b);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|